#include "pgr.h"
#include "sphere.h"
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;

// Data
struct Point {
	float lmtexcoord[2];
    float texcoord[2];
    float position[3];
} const houseVertices[] = {
	//floor
	{ { 0.25, 1.0 },  { 0.0, 0.0 }, { -55.0, -55.0, -55.0 } }, 
    { { 0.25, 0.0 },  { 0.0, 4.0 }, { -55.0, -55.0,  55.0 } }, 
    { { 0.5, 0.0 },   { 4.0, 4.0 }, {  55.0, -55.0,  55.0 } }, 
    { { 0.5, 1.0 },   { 4.0, 0.0 }, {  55.0, -55.0, -55.0 } },
};

// floor indices
const unsigned char house[] = {
     0,  1,  2,
     0,  2,  3,
     4,  5,  6,
     4,  6,  7,
     8,  9, 10,
     8, 10, 11,
    12, 13, 14,
    12, 14, 15,
};

const float lightquad[][3] = {
    {0, 0, 0},
    {0, 0, 0},
    { 0, 0, 0},
    { 0, 0, 0}
};

glm::vec3 lightPos = glm::vec3(9, 4.97f, 9);

//Light color properties
glm::vec3 lightAmbientColor =  glm::vec3(0.8);
glm::vec3 lightDiffuseColor =  glm::vec3(0, 1, 0);
glm::vec3 lightSpecularColor = glm::vec3(0.3, 0.7, 0.3);
GLuint HouseVBO, HouseEBO;
GLuint SphereVBO, SphereEBO;
GLuint lightquadVBO;
GLuint texture;
GLuint samplerTiles, samplerLightmap;

int width, height;
float rx = 90.0f, ry = 0.0f, pz = 20.0f;

// Shaders

//floor
GLuint HVS, HFS, HProg;
GLuint HpositionAttrib, HtcAttrib, HmvpUniform;
GLuint HtextureUniform;
GLuint lightmapTexture;
GLint  LMUniform;
GLuint LMCoordAttrib;

//Sphere
GLuint SVS, SFS, SProg;
GLuint SpositionAttrib, SnormalAttrib, SmUniform, SlightPosUniform, SvpUniform, ScamPosUniform;
GLuint SlaUniform, SldUniform, SlsUniform;

//Light
GLuint lightVS, lightFS, lightProg;
GLuint lightMvpUniform, lightPosAttrib, lightAmbientUniform, lightDiffuseUniform;

// Event handlers

void onInit()
{
    // Shaders
    HVS = compileShader(GL_VERTEX_SHADER, loadFile("data\\house.vs").c_str());
    HFS = compileShader(GL_FRAGMENT_SHADER, loadFile("data\\house.fs").c_str());
    HProg = linkShader(2, HVS, HFS);
    HpositionAttrib = glGetAttribLocation(HProg, "position");
    HtcAttrib = glGetAttribLocation(HProg, "tc");
	LMCoordAttrib = glGetAttribLocation(HProg, "lm_tc");
    HmvpUniform = glGetUniformLocation(HProg, "mvp");
    HtextureUniform = glGetUniformLocation(HProg, "tex");
    lightVS = compileShader(GL_VERTEX_SHADER, loadFile("data\\light.vs").c_str());
    lightFS = compileShader(GL_FRAGMENT_SHADER, loadFile("data\\light.fs").c_str());
    lightProg = linkShader(2, lightVS, lightFS);
    lightMvpUniform = glGetUniformLocation(lightProg, "mvp");
    lightAmbientUniform = glGetUniformLocation(lightProg, "lightAmbientColor");
    lightDiffuseUniform = glGetUniformLocation(lightProg, "lightDiffuseColor");
    lightPosAttrib = glGetAttribLocation(lightProg, "position");
	LMUniform = glGetUniformLocation(HProg, "lightMap");
    SVS = compileShader(GL_VERTEX_SHADER, loadFile("data\\sphere.vs").c_str());
    SFS = compileShader(GL_FRAGMENT_SHADER, loadFile("data\\sphere.fs").c_str());
    SProg = linkShader(2, SVS, SFS);
    SpositionAttrib = glGetAttribLocation(SProg, "position");
    SnormalAttrib = glGetAttribLocation(SProg, "normal");
    SvpUniform = glGetUniformLocation(SProg, "vp");
	SmUniform = glGetUniformLocation(SProg, "m");
    SlightPosUniform = glGetUniformLocation(SProg, "lightPos");
    ScamPosUniform = glGetUniformLocation(SProg, "camPos");
    SlaUniform = glGetUniformLocation(SProg, "lightColorAmbient");
    SldUniform = glGetUniformLocation(SProg, "lightColorDiffuse");
    SlsUniform = glGetUniformLocation(SProg, "lightColorSpecular");

    // Copy house to graphics card
    glGenBuffers(1, &HouseVBO);
    glBindBuffer(GL_ARRAY_BUFFER, HouseVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(houseVertices), houseVertices, GL_STATIC_DRAW);
    glGenBuffers(1, &HouseEBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, HouseEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(house), house, GL_STATIC_DRAW);

    // Copy sphere to graphics card
    glGenBuffers(1, &SphereVBO);
    glBindBuffer(GL_ARRAY_BUFFER, SphereVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVertices), sphereVertices, GL_STATIC_DRAW);
    glGenBuffers(1, &SphereEBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, SphereEBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere), sphere, GL_STATIC_DRAW);

    // Light quad
    glGenBuffers(1, &lightquadVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, lightquadVBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(lightquad), lightquad, GL_STATIC_DRAW);

    //Load texture from file
	SDL_PixelFormat pixFormat;
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	memset(&pixFormat, 0, sizeof(SDL_PixelFormat));
	pixFormat.BitsPerPixel = 24;
	pixFormat.BytesPerPixel = 3;
	pixFormat.Rmask = 0xff;
	pixFormat.Gmask = 0xff00;
	pixFormat.Bmask = 0xff0000;

	SDL_Surface * surface = SDL_ConvertSurface(SDL_LoadBMP("data/tiles.bmp"), &pixFormat, 0);
    if(surface == NULL) throw SDL_Exception();

	//Creating diffuse texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, surface->w, surface->h, 0, GL_RGB, GL_UNSIGNED_BYTE, surface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
	glGenSamplers(1, &samplerTiles);
	glSamplerParameteri(samplerTiles, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glSamplerParameteri(samplerTiles, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glSamplerParameteri(samplerTiles, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glSamplerParameteri(samplerTiles, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	//load image
	unsigned char* photo_floor = SOIL_load_image("icons/bowlink.jpg", &width, &height, 0, SOIL_LOAD_RGB);
	glGenTextures(1, &lightmapTexture);
	glBindTexture(GL_TEXTURE_2D, lightmapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, photo_floor);
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);
	glGenSamplers(1, &samplerLightmap);
	glSamplerParameteri(samplerLightmap, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glSamplerParameteri(samplerLightmap, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glSamplerParameteri(samplerLightmap, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glSamplerParameteri(samplerLightmap, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

}

void onWindowRedraw()
{   // t - absolute time, dt - time step
	static float t0 = SDL_GetTicks() / 5000.0f;
	static float t = t0;
	float t1 = SDL_GetTicks() / 5000.0f;
	float dt = t1 - t;
	t = t1;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.3f, 0.3f, 0.2f, 1.0f);//background
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    
    //Setup orbital camera
    float radx = glm::radians(rx);
    float rady = glm::radians(ry);
    float x = pz * cos(rady) * cos(radx);
    float y = pz * sin(rady);
    float z = pz * cos(rady) * sin(radx);
    glm::vec4 camPos = glm::vec4(x, y, z, 1);
	glm::mat4 view = glm::lookAt(glm::vec3(camPos), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
    glm::mat4 persp = glm::perspective(45.0f, (float)width/(float)height, 1.0f, 1000.0f);
	glm::mat4 houseMvp = persp*view; //no model matrix (=identity) -> object space = world space

    //Draw house
    glUseProgram(HProg);
    glUniformMatrix4fv(HmvpUniform, 1, GL_FALSE, glm::value_ptr(houseMvp));

    //Diffuse texture - bound to texturing unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
	glBindSampler(0, samplerTiles);
    glUniform1i(HtextureUniform, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, lightmapTexture);
	glBindSampler(1, samplerLightmap);
	glUniform1i(LMUniform, 1);
    glEnableVertexAttribArray(HpositionAttrib);
    glEnableVertexAttribArray(HtcAttrib);
	if(LMUniform>=0)
		glEnableVertexAttribArray(LMCoordAttrib);

	//Specifying data input, assigning buffers to attributes
    glBindBuffer(GL_ARRAY_BUFFER, HouseVBO);
    glVertexAttribPointer(HpositionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Point), (void*)offsetof(Point, position));
    glVertexAttribPointer(HtcAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Point), (void*)offsetof(Point, texcoord));
	
	if (LMUniform >= 0)
		glVertexAttribPointer(LMCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Point), (void*)offsetof(Point, lmtexcoord));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, HouseEBO);
    glDrawElements(GL_TRIANGLES, sizeof(house)/sizeof(*house), GL_UNSIGNED_BYTE, NULL);
    glDisableVertexAttribArray(HpositionAttrib);
    glDisableVertexAttribArray(HtcAttrib);
	glDisableVertexAttribArray(LMCoordAttrib);

    //Draw "light"
    glUseProgram(lightProg);
    glEnableVertexAttribArray(lightPosAttrib);
    glm::mat4 lightMVP = persp * view * glm::translate(lightPos);
    glUniformMatrix4fv(lightMvpUniform, 1, GL_FALSE, glm::value_ptr(lightMVP));
    glUniform3fv(lightAmbientUniform, 1, glm::value_ptr(lightAmbientColor));
    glUniform3fv(lightDiffuseUniform, 1, glm::value_ptr(lightDiffuseColor));
    glBindBuffer(GL_ARRAY_BUFFER, lightquadVBO);
    glVertexAttribPointer(lightPosAttrib, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDisableVertexAttribArray(lightPosAttrib);
    
    // Draw spheres
    glUseProgram(SProg);
    glEnableVertexAttribArray(SpositionAttrib);
    glEnableVertexAttribArray(SnormalAttrib);
    glBindBuffer(GL_ARRAY_BUFFER, SphereVBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, SphereEBO);
    glVertexAttribPointer(SpositionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(SphereVertex), (void*)offsetof(SphereVertex, position));
    glVertexAttribPointer(SnormalAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(SphereVertex), (void*)offsetof(SphereVertex, normal));
    glUniform3fv(ScamPosUniform, 1, glm::value_ptr(camPos));
    glUniformMatrix4fv(SvpUniform, 1, GL_FALSE, glm::value_ptr(persp * view));
    glUniform3fv(SlaUniform, 1, glm::value_ptr(lightAmbientColor));
    glUniform3fv(SldUniform, 1, glm::value_ptr(lightDiffuseColor));
    glUniform3fv(SlsUniform, 1, glm::value_ptr(lightSpecularColor));

    // Sphere 1
    glm::mat4 s1ModelMatrix = glm::translate(glm::vec3(2.1, 0.9, 0.9)) * glm::scale(glm::vec3(1));
    glUniformMatrix4fv(SmUniform, 1, GL_FALSE, glm::value_ptr(s1ModelMatrix));
    glUniform3fv(SlightPosUniform, 1, glm::value_ptr(lightPos));
    glDrawElements(GL_TRIANGLES, sizeof(sphere)/sizeof(**sphere), sphereIndexType, NULL);

    // Sphere 2
	glm::mat4 s2ModelMatrix = glm::mat4(1);
	glm::mat4 tr2 = glm::translate(glm::vec3(4, 0, 0));
	glm::mat4 r2 = glm::rotate(s1ModelMatrix, t * glm::radians(360.0f), glm::vec3(0, 1, 0));
	glm::mat4 s2 = glm::scale(glm::vec3(1, 1, 1));
	s2ModelMatrix = r2 * tr2 * s2;
    glUniformMatrix4fv(SmUniform, 1, GL_FALSE, glm::value_ptr(s2ModelMatrix));
    glUniform3fv(SlightPosUniform, 1, glm::value_ptr(lightPos));
    glDrawElements(GL_TRIANGLES, sizeof(sphere)/sizeof(**sphere), sphereIndexType, NULL);

	// Sphere 3
	glm::mat4 s3ModelMatrix = glm::mat4(1);
	glm::mat4 tr3 = glm::translate(glm::vec3(4, 0, 0));
	glm::mat4 r3 = glm::rotate(s2ModelMatrix, t * glm::radians(360.0f), glm::vec3(0, 1, 1));
	glm::mat4 s3 = glm::scale(glm::vec3(1, 1, 1));
	s3ModelMatrix = r3 * tr3 * s3;
	glUniformMatrix4fv(SmUniform, 1, GL_FALSE, glm::value_ptr(s3ModelMatrix));
	glUniform3fv(SlightPosUniform, 1, glm::value_ptr(lightPos));
	glDrawElements(GL_TRIANGLES, sizeof(sphere) / sizeof(**sphere), sphereIndexType, NULL);

	//Sphere 4
	glm::mat4 s4ModelMatrix = glm::mat4(1);
	glm::mat4 tr4 = glm::translate(glm::vec3(4, 0, 0));
	glm::mat4 r4 = glm::rotate(s3ModelMatrix, t * glm::radians(360.0f), glm::vec3(0, 1, 1));
	glm::mat4 s4 = glm::scale(glm::vec3(1, 1, 1));
	s4ModelMatrix = r4 * tr4 * s4;
	glUniformMatrix4fv(SmUniform, 1, GL_FALSE, glm::value_ptr(s4ModelMatrix));
	glUniform3fv(SlightPosUniform, 1, glm::value_ptr(lightPos));
	glDrawElements(GL_TRIANGLES, sizeof(sphere) / sizeof(**sphere), sphereIndexType, NULL);

	//Sphere 5
	glm::mat4 s5ModelMatrix = glm::mat4(1);
	glm::mat4 tr5 = glm::translate(glm::vec3(4, 0, 0));
	glm::mat4 r5 = glm::rotate(s4ModelMatrix, t * glm::radians(360.0f), glm::vec3(0, 1, 1));
	glm::mat4 s5 = glm::scale(glm::vec3(1, 1, 1));
	s5ModelMatrix = r5 * tr5 * s5;
	glUniformMatrix4fv(SmUniform, 1, GL_FALSE, glm::value_ptr(s5ModelMatrix));
	glUniform3fv(SlightPosUniform, 1, glm::value_ptr(lightPos));
	glDrawElements(GL_TRIANGLES, sizeof(sphere) / sizeof(**sphere), sphereIndexType, NULL);

	//
    glDisableVertexAttribArray(SnormalAttrib);
    glDisableVertexAttribArray(SpositionAttrib);
    
    SDL_GL_SwapBuffers();
}

void onWindowResized(int w, int h)
{
    glViewport(0, 0, w, h);
    width = w; height = h;
}

void onKeyDown(SDLKey key, Uint16 /*mod*/)
{
    switch(key) {
        case SDLK_ESCAPE : quit(); return;
        default : return;
    }
}

void onKeyUp(SDLKey /*key*/, Uint16 /*mod*/)
{
}

void onMouseMove(unsigned /*x*/, unsigned /*y*/, int xrel, int yrel, Uint8 buttons)
{
    if(buttons & SDL_BUTTON_LMASK)
    {
        rx += xrel;
        ry += yrel;
        redraw();
    }
    if(buttons & SDL_BUTTON_RMASK)
    {
        pz += yrel;
        if (pz < 0)
            pz = 0;
        redraw();
    }
}

void onMouseDown(Uint8 /*button*/, unsigned /*x*/, unsigned /*y*/)
{
}

void onMouseUp(Uint8 /*button*/, unsigned /*x*/, unsigned /*y*/)
{
}

// Main
int main(int /*argc*/, char ** /*argv*/)
{
    try {
        // Init SDL - only video subsystem will be used
        if(SDL_Init(SDL_INIT_VIDEO) < 0) throw SDL_Exception();

        // Shutdown SDL when program ends
        atexit(SDL_Quit);

        init(800, 600, 24, 16, 0);

        mainLoop();

    } catch(exception & ex) {
        cout << "ERROR : " << ex.what() << endl;
        cout << "Press any key to exit...\n";
        cin.get();

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
