#version 130

uniform vec3 lightPos;
uniform vec3 camPos;
uniform vec3 lightColorAmbient;
uniform vec3 lightColorDiffuse;
uniform vec3 lightColorSpecular;
out vec4 fragColor; 
in vec3 n;
in vec4 wpos;

void main()
{ 
	//fragColor = vec4(color, color, color, 1);
	//compute lighting in fragment shader

	//define material properties
	//ambient, diffuse, specular color shininess
	vec3 mA = vec3(0.5, 0.5, 0.5);
	vec3 mD = vec3(0.5, 0.5, 0.5);
	vec3 mS = vec3(0.5, 0.5, 0.5);
	float shininess = 25;
	
	//calculate ambient lighting
	vec3 c = lightColorAmbient*mA;

	//calculate diffuse lighting
    vec3 lpos = lightPos - (wpos/wpos.w).xyz;
	float diffuse = dot(normalize(n), normalize(lpos));
	if(diffuse>0)
	{
		c += diffuse * lightColorDiffuse * mD;
		
		//calculate specular lighting specular
		float specular = pow(dot(normalize(camPos-wpos.xyz), reflect(normalize(-lpos),normalize(n))),shininess);
		if(specular>0)
		{
			c += specular*lightColorSpecular*mS;
		}
	}
	//assign fragment color
	fragColor = vec4(c, 1.0f);
}
