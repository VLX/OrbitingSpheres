#version 140

in vec3 position;
in vec3 normal;
uniform mat4 vp;
uniform mat4 m;
out vec3 n;
out vec4 wpos;

void main() 
{ 
	gl_Position = vp*m*vec4(position,1);
	n = normalize(inverse(transpose(m))*vec4(normal,0.0f)).xyz;
	wpos = m*vec4(position, 1.0f);
}